$(document).ready(function() {
  $("#slides1").slides({//slider with introduction
    container: 'slides_container',
    play: 10000,
    generatePagination: true,
    paginationClass: 'pagination',
    currentClass: 'current'
  });

  var isChrome = window.chrome;//because smooth scrolling doesn't work on chrome
  if(!isChrome){
    $.localScroll();//back to top button
  };

  $("#toTop").css("display", "none");
  $(window).scroll(function(){
    if($(window).scrollTop() > 0){
      $("#toTop").fadeIn("slow");
    }
    else {
      $("#toTop").fadeOut("slow");
    }
  });
  $("#toTop").click(function(e){
    e.preventDefault();
    $("html, body").animate({scrollTop:0},"slow");
  });

  $('.thumbnail').hover(function(){
    $(this).children('a').toggle();
  });

  /*
  var myWidth = 0, myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }
  $('.bg_box').height(myHeight);
  $('.photo_bg_box').height(myHeight);
  
  $(window).resize(function(){
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
      //Non-IE
      myWidth = window.innerWidth;
      myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
      //IE 6+ in 'standards compliant mode'
      myWidth = document.documentElement.clientWidth;
      myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
      //IE 4 compatible
      myWidth = document.body.clientWidth;
      myHeight = document.body.clientHeight;
    }
    $('.bg_box').height(myHeight);
    $('.photo_bg_box').height(myHeight);
  });*/

});