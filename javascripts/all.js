// Put this file in your javascripts directory
// Iterate over the items in the project and concatenate the javascripts that are marked for compression.
<% for js_item in @items.select {|i| i.identifier =~ %r{^/javascripts/}} -%>
	<% next unless js_item.attributes.fetch(:compressed, true) -%>
	<%= js_item.rep_named(:default).compiled_content %>
<% end -%>